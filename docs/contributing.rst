Contributing
--------------

If you have a request for an additional feature, spot any mistake or find any problem with using the code, please open an issue.
We follow a standard {create a branch-apply your edits-submit a merge request} workflow to develop the code.
Specifically if you want to contribute to the development of the code, please follow the steps described below:

1. Make sure to start from the latest version of the branch you are looking at, so please pull before planning any changes to the code;
2. Create your own branch (if you have permissions below developer, please fork the code and then keep following these instructions) where you can apply all your improvements and modifications to the code;
3. Please check thoroughly your changes. Specifically check that, after applying them, you are still able to install and run the code without occurring in any errors. Test it by running a couple of quick examples (e.g. config_gw150914.ini, config_damped_sinusoids_inj.ini).
4. Create a merge request to master and assign it to either Gregorio (gregorio.carullo@ligo.org) or Danny (danny.laghi@ligo.org);

Someone will have a look at your code and eventually merge it.
Thanks for taking time to contribute to the code, your help is greatly appreciated!

For additional questions, feedback and suggestions feel free to reach by email to `gregorio.carullo@ligo.org` or, if you are part of the LVK collaboration, directly through `chat.ligo.org`. 
