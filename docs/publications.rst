References
-------------

- Methods:

   The code is mainly based on the formalism described in `this reference <https://arxiv.org/abs/1902.07527>`_.
   For signals extending below a pure ringdown emission, it uses the truncated formulation of `this reference <https://arxiv.org/abs/1905.00869>`_.   
   Additional details on the methodology and results fo GWTC-2 events can be found in the `GWTC-2 Testing GR LVK catalog <https://arxiv.org/abs/2010.14529>`_.

- Additional studies using pyRing:

   This is a set of additional published studies employing pyRing:  
   
   * GW190521: `discovery <https://arxiv.org/abs/2009.01075>`_ and `implications <https://arxiv.org/abs/2009.01190>`_ papers;
   * Investigations and observational constraints on the area quantisation hypothesis: `arXiv <https://arxiv.org/abs/2011.03816>`_; 
   * Constraints on alternative theories of gravity using the ParSpec formalism: `arXiv <https://arxiv.org/abs/2102.05939>`_;  
   * Constraints on braneworld gravity: `arXiv <https://arxiv.org/abs/2106.05558>`_;
   * Verification of the Bekenstein-Hod bound: `arXiv <https://arxiv.org/abs/2103.06167>`_;
   * Probing the Purely Ingoing Nature of the Black-hole Event Horizon `arXiv <https://arxiv.org/abs/1912.07058>`_;
   * Spectroscopy of Rotating Black Holes Pierced by Cosmic Strings: `arXiv <https://arxiv.org/abs/2002.01695>`_.

- Citing ``pyRing``:
   If you use ``pyRing`` in your research, please cite the methods papers discussed above (`[1] <https://arxiv.org/abs/1902.07527>`_, `[2] <https://arxiv.org/abs/1905.00869>`_, `[3] <https://arxiv.org/abs/2010.14529>`_).
